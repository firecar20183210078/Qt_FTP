QT+= core gui network
QT+=widgets
CONFIG += console
TEMPLATE = app
TARGET = ../output/main
INCLUDEPATH += .
INCLUDEPATH += D:\opencv\opencv\build\include 
INCLUDEPATH += .\headfile 

LIBS += D:\qt\opencv\lib\libopencv_*.a      

DEFINES += QT_DEPRECATED_WARNINGS

# Input
SOURCES += src/*.cpp
HEADERS += headfile/*.h 
FORMS += UI/*.ui

RESOURCES += \
    res.qrc \
    res.qrc