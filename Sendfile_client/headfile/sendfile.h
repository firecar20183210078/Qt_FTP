#ifndef SENDFILE_H
#define SENDFILE_H

#include <QObject>
#include <QTcpSocket>

class SendFile : public QObject
{
    Q_OBJECT
public:
    explicit SendFile(QObject *parent = nullptr);
    ~SendFile(); 
    void connectServer(QString ip, unsigned short port); 
    void sendFile(QString path); 
    void close(); 

signals:
    void connect_OK(); 
    void disconnect_OK(); 
    void openfile_error(); 
    void send_percent(int); 
private:
    QTcpSocket *sfd = nullptr;
};

#endif // SENDFILE_H