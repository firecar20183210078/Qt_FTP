#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QLabel>
#include <QThread>
#include "sendfile.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void init(); 
    ~MainWindow();

signals:
    void startconnect(QString ip, unsigned short port); 
    void startsendfile(QString path); 
    void sfdclose(); 

private:
    Ui::MainWindow *ui;
    QTcpSocket *sfd = nullptr; 
    QThread *t1 = nullptr; 
    SendFile *work_1 = nullptr; 
};

#endif // MAINWINDOW_H
