#include <QTcpSocket>
#include <QFileDialog>
#include <QMessageBox>
#include <QHostAddress>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qDebug() << "当前主线程:" << QThread::currentThread(); 
    ui->setupUi(this);
    ui->lineEdit_ip->setText("127.0.0.1"); 
    ui->lineEdit_port->setText("7777"); 
    ui->btn_send->setEnabled(false); 
    ui->btn_disconnect->setEnabled(false);  

    connect(ui->btn_connect, &QPushButton::clicked, this, [=]()
    {
        ui->btn_connect->setEnabled(false); 
        qDebug() << "btn_connect_clicked 当前线程地址:" << QThread::currentThread(); 
        this->sfd = new QTcpSocket; 
        this->t1 = new QThread; 
        this->work_1 = new SendFile; 
        qDebug() << "1.地址: " << t1 << "   " << work_1; 
        this->work_1->moveToThread(this->t1); 
        //t1->start(); 
        qDebug() << "btn_connect clicked"; 
        QString ip = ui->lineEdit_ip->text(); 
        unsigned short port = ui->lineEdit_port->text().toUShort(); 
        this->init(); 
        t1->start(); 
        emit startconnect(ip, port);
    }); 
    
    // 选择文件按钮 
    connect(ui->btn_file, &QPushButton::clicked, this, [=]()
    {
        QString path = QFileDialog::getOpenFileName(this, "选择要发送的文件", "./"); 
        ui->lineEdit_file->setText(path); 
        ui->progressBar->setValue(0); 
    }); 

    // 发送文件按钮 
    connect(ui->btn_send, &QPushButton::clicked, this, [=]()
    {
        QString path = ui->lineEdit_file->text(); 
        if(path.isEmpty()) 
        {
            QMessageBox::warning(this, "错误", "未选择任何文件"); 
            return;
        }
        emit startsendfile(path); 
    }); 

    // 断开连接按钮 
    connect(ui->btn_disconnect, &QPushButton::clicked, this, [=]()
    {
        qDebug() << "btn_disconnect当前线程地址:" << QThread::currentThread(); 
        qDebug() << "2.地址: " << this->t1 << "   " << this->work_1; 
        qDebug() << "disconnect0"; 
        emit sfdclose(); 
        qDebug() << "disconnect1"; 
        ui->btn_send->setEnabled(false); 
        ui->btn_connect->setEnabled(true);  
        ui->btn_disconnect->setEnabled(false); 
        this->t1->quit();
        qDebug() << "disconnect2"; 
        this->t1->wait(); 
        qDebug() << "disconnect3"; 
        //this->t1->deleteLater(); 
        delete t1; 
        qDebug() << "disconnect4"; 
        //this->work_1->deleteLater();
        delete work_1; 
        qDebug() << "disconnect5"; 
        this->t1 = nullptr; 
        this->work_1 = nullptr; 
    }); 
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::init() 
{
    if(!this->work_1 || !this->t1) return; 
    connect(this, &MainWindow::startconnect, this->work_1, &SendFile::connectServer); 
    connect(this, &MainWindow::startsendfile, this->work_1, &SendFile::sendFile); 
    connect(this, &MainWindow::sfdclose, this->work_1, &SendFile::close); 

    // 处理子线程发送的信号 
    connect(work_1, &SendFile::connect_OK, this, [=]()
    {
        qDebug() << "3.connect地址: " << this->t1 << "   " << this->work_1; 
        ui->btn_connect->setEnabled(false); 
        ui->btn_send->setEnabled(true); 
        ui->btn_disconnect->setEnabled(true); 
        QMessageBox::information(this, "连接服务器", "已经成功连接服务器..."); 
    }); 

    connect(work_1, &SendFile::disconnect_OK, this, [=]()
    {
        qDebug() << "disconnect_OK"; 
        if(this->t1 && this->work_1)
        {
            ui->btn_disconnect->setEnabled(false); 
            ui->btn_send->setEnabled(false); 
            ui->btn_connect->setEnabled(true);  
            t1->quit();
            t1->wait(); 
            work_1->deleteLater();
            t1->deleteLater(); 
        }
        QMessageBox::information(this, "断开连接", "已与服务器断开连接"); 
    }); 

    connect(work_1, &SendFile::openfile_error, this, [=]()
    {
        QMessageBox::warning(this, "打开文件失败", "请检测文件的有效路径"); 
    }); 


    connect(this->work_1, &SendFile::send_percent, this, [=](int percent)
    {
        ui->progressBar->setValue(percent); 
        if(percent == 100) 
        {
            QMessageBox::information(this, "成功", "文件已成功发送"); 
            ui->progressBar->setValue(0); 
        }
    }); 
}