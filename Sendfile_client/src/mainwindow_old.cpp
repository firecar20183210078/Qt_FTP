#if 0
#include <QTcpSocket>
#include <QFileDialog>
#include <QMessageBox>
#include <QThread>
#include "sendfile.h"
#include <QHostAddress>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qDebug() << "当前主线程:" << QThread::currentThread(); 
    ui->setupUi(this);
    ui->lineEdit_ip->setText("127.0.0.1"); 
    ui->lineEdit_port->setText("7777"); 
    ui->btn_send->setEnabled(false); 
    ui->btn_disconnect->setEnabled(false);  

    connect(ui->btn_connect, &QPushButton::clicked, this, [=]()
    {
        qDebug() << "btn_connect_clicked 当前线程地址:" << QThread::currentThread(); 
        this->sfd = new QTcpSocket; 
        QThread *t1 = new QThread; 
        SendFile *work_1 = new SendFile; 
        qDebug() << "1.地址: " << t1 << "   " << work_1; 
        work_1->moveToThread(t1); 
        //t1->start(); 
        qDebug() << "btn_connect clicked"; 
        QString ip = ui->lineEdit_ip->text(); 
        unsigned short port = ui->lineEdit_port->text().toUShort(); 

        connect(this, &MainWindow::startconnect, work_1, &SendFile::connectServer); 
        connect(this, &MainWindow::startsendfile, work_1, &SendFile::sendFile); 
        connect(this, &MainWindow::sfdclose, work_1, &SendFile::close); 

        // 处理子线程发送的信号 
        connect(work_1, &SendFile::connect_OK, this, [=]()
        {
            qDebug() << "3.connect地址: " << t1 << "   " << work_1; 
            ui->btn_connect->setEnabled(false); 
            ui->btn_send->setEnabled(true); 
            ui->btn_disconnect->setEnabled(true); 
            QMessageBox::information(this, "连接服务器", "已经成功连接服务器..."); 
        }); 

        #if 0
        connect(work_1, &SendFile::disconnect_OK, this, [=]()
        {
            ui->btn_send->setEnabled(false); 
            ui->btn_connect->setEnabled(true);  
            t1->quit();
            t1->wait(); 
            work_1->deleteLater();
            t1->deleteLater(); 
        }); 
        #endif  

        connect(work_1, &SendFile::openfile_error, this, [=]()
        {
            QMessageBox::warning(this, "打开文件失败", "请检测文件的有效路径"); 
        }); 


        connect(work_1, &SendFile::send_percent, this, [=](int percent)
        {
            ui->progressBar->setValue(percent); 
        }); 
    
        // 断开连接按钮 
        connect(ui->btn_disconnect, &QPushButton::clicked, this, [=]()
        {
            qDebug() << "btn_disconnect当前线程地址:" << QThread::currentThread(); 
            qDebug() << "2.地址: " << t1 << "   " << work_1; 
            qDebug() << "disconnect0"; 
            emit sfdclose(); 
            qDebug() << "disconnect1"; 
            ui->btn_send->setEnabled(false); 
            ui->btn_connect->setEnabled(true);  
            ui->btn_disconnect->setEnabled(false); 
            t1->quit();
            qDebug() << "disconnect2"; 
            t1->wait(); 
            qDebug() << "disconnect3"; 
            //t1->deleteLater(); 
            //delete t1; 
            qDebug() << "disconnect4"; 
            //work_1->deleteLater();
            //delete work_1; 
            qDebug() << "disconnect5"; 
        }); 

        t1->start(); 
        emit startconnect(ip, port);
    }); 

    
    // 选择文件按钮 
    connect(ui->btn_file, &QPushButton::clicked, this, [=]()
    {
        QString path = QFileDialog::getOpenFileName(this, "选择要发送的文件", "./"); 
        ui->lineEdit_file->setText(path); 
        ui->progressBar->setValue(0); 
    }); 

    // 发送文件按钮 
    connect(ui->btn_send, &QPushButton::clicked, this, [=]()
    {
        QString path = ui->lineEdit_file->text(); 
        if(path.isEmpty()) 
        {
            QMessageBox::warning(this, "错误", "未选择任何文件"); 
            return;
        }
        emit startsendfile(path); 
    }); 

}

MainWindow::~MainWindow()
{
    delete ui;
}

#endif 