#ifndef RECVFILE_H
#define RECVFILE_H

#include <QObject>
#include <QThread>
#include <QTcpSocket>
#include <QFile> 

class RecvFile : public QThread
{
    Q_OBJECT
public:
    explicit RecvFile(QTcpSocket *cfd, QObject *parent = nullptr);
    ~RecvFile(); 

protected:
    void run() override; 

signals:
    void openfile_error(); 
    void recv_ok(); 
    void disconnect(); 

private:
    QTcpSocket *cfd = nullptr;
    QFile *file = nullptr; 
    int file_size = 0; 
    int recv_size = 0; 
};

#endif // SENDFILE_H