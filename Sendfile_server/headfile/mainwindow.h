#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTcpServer>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void startconnect(QString ip, unsigned short port); 
    void startsendfile(QString path); 

private:
    Ui::MainWindow *ui;
    QTcpServer *lfd;
//    QTcpSocket *cfd; 
    QLabel *status;
};

#endif // MAINWINDOW_H
